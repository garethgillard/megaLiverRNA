
*To do:*  

Make sure that there are no other important liver RNAseq datasets we want to add (can you talk to Yang/Tom?)  

We have to make a metadata-file that allows us to rather quickly do some dataset stats. This means going beyond the information we automatically get from array express, and compile a user friendly file, including the following info for each sample.  
    We need to link samples to an experiment (this is available in your file already)  
    We need a unified way of coding a condition within an experiment so we can do effective calculations of summary stats across biological reps  
    We need to assign fresh/saltwater for all samples  
    We need info if the experiment has a dietary experimental manipulation  
    …and a column with abbreviations for feed types (including coding ‘standard’ if no particular dietary manipulation is done and the fish is given a standard salmon aquaculture feed)  
