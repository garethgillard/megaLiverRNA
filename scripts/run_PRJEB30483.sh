#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --partition=smallmem
#SBATCH --output=../logs/log-PRJEB30483-%j.out

module load Anaconda3
module load snakemake
source activate cutadaptenv

## Create index
# mkdir "index"
# curl https://ftp.ncbi.nlm.nih.gov/genomes/all/annotation_releases/8030/100/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_rna.fna.gz -o index/GCF_000233375.1_ICSASG_v2_rna.fna.gz
# curl https://ftp.ncbi.nlm.nih.gov/genomes/all/annotation_releases/8030/100/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_genomic.fna.gz -o index/GCF_000233375.1_ICSASG_v2_genomic.fna.gz
# grep "^>" <(gunzip -c index/GCF_000233375.1_ICSASG_v2_genomic.fna.gz) | cut -d " " -f 1 > index/decoys.txt
# sed -i.bak -e 's/>//g' index/decoys.txt
# cat index/GCF_000233375.1_ICSASG_v2_rna.fna.gz index/GCF_000233375.1_ICSASG_v2_genomic.fna.gz > index/gentrome.fa.gz
# /mnt/users/garethg/bin/salmon/bin/salmon index -t index/gentrome.fa.gz -d index/decoys.txt -p 20 -i index/GCF_000233375.1_ICSASG_v2 --gencode

snakemake --keep-going --cores 24 -s snakefile.py --configfile config.yaml --config SAMPLE_FILE=../data/PRJEB30483_YangFryLiver_SE.tsv PAIRED_READS=False OUT_DIR=/mnt/SCRATCH/garethg/megaLiverNetwork/PRJEB30483
