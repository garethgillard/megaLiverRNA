import pandas as pd
import os.path

# Samples
samples = pd.read_csv(config["SAMPLE_FILE"],sep="\t",dtype = str)
# if ('sample' not in list(samples.columns)) or ('name' not in list(samples.columns)) or ('R1/R2' not in list(samples.columns)):
#     sys.exit("Your sample file "+config["SAMPLE_FILE"]+" do not contain at least 3 columns sample, R1/R2, and name. Note that this file must be tab delimited.")
ALL_SAMPLES = samples['sample'].unique().tolist()
PAIRED_READS = config["PAIRED_READS"]
LOCAL_PATH = config["LOCAL_PATH"]
DOWNLOAD = config["DOWNLOAD"]

# Define output dirs
OUT_DIR = config["OUT_DIR"]
FASTQ_DIR = OUT_DIR+"/"+config["FASTQ_SUBDIR"]
FASTQC_DIR = OUT_DIR+"/"+config["FASTQC_SUBDIR"]
TRIMMED_DIR = OUT_DIR+"/"+config["TRIMMED_FASTQ_SUBDIR"]
SALMON_DIR = OUT_DIR+"/"+config["SALMON_SUBDIR"]

# Reference index path
INDEX = config["INDEX_SUBDIR"]+"/"+config["REF"]

# Define paths to program
FASTQC_PROGRAM = config["FASTQC_PROGRAM"]
TRIMGALORE_PROGRAM = config["TRIMGALORE_PROGRAM"]
CUTADAPT_PROGRAM = config["CUTADAPT_PROGRAM"]
SALMON_PROGRAM =  config["SALMON_PROGRAM"]
ENABROWSER_PROGRAM = config["ENABROWSER_PROGRAM"]

# # Data is paired end or single end
# R12 = list(samples['R1/R2'].unique())
# if (R12 != ["R1","R2"] and R12 != ["SE"]):
#     sys.exit("Error in the tsv file: column R1/R2 should contains value R1 or R2 for paired end data - and SE for single end data")
# if len(R12)==2:
#     PE = True
# else:
#     PE = False

# Number of threads for each task
CUTADAPTER_THREADS = config["CUTADAPTER_THREADS"]
SALMON_THREADS = config["SALMON_THREADS"]

# Parameter for cutadapt
SKIP_TRIM = config["SKIP_TRIM"]
MIN_LENGTH = config["MIN_LENGTH"]
MIN_QUALITY = config["MIN_QUALITY"]
ADAPTER_SEQUENCE = config["ADAPTER_SEQUENCE"]
STRINGENCY = config["STRINGENCY"]
TRIM_N = config["TRIM_N"]
ERROR_RATE = config["ERROR_RATE"]
CUTADAPTER_PROGRAM = "TRIMGALORE"

# Define final output files for the pipeline
OUT = [SALMON_DIR+"/"+str(F)+".quant" for F in ALL_SAMPLES]
SE_OUT = OUT+[TRIMMED_DIR+"/"+str(F)+"_trimmed.fastq.gz" for F in ALL_SAMPLES]
PE_OUT = OUT+[TRIMMED_DIR+"/"+str(F)+"_trimmed_R1.fastq.gz" for F in ALL_SAMPLES]+[TRIMMED_DIR+"/"+str(F)+"_trimmed_R2.fastq.gz" for F in ALL_SAMPLES]
if (not(config["SKIP_FASTQC_RAW"])):
    SE_OUT = SE_OUT+[FASTQC_DIR+"/rawData/"+str(F)+"_fastqc.html" for F in ALL_SAMPLES] #fastqc report for raw data
    PE_OUT = PE_OUT+[FASTQC_DIR+"/rawData/"+str(F)+"_R1_fastqc.html" for F in ALL_SAMPLES]+[FASTQC_DIR+"/rawData/"+str(F)+"_R2_fastqc.html" for F in ALL_SAMPLES] #fastqc report for raw data
    TRIMMED_DIR0 = TRIMMED_DIR+"tmp"
    TRIMMED_DIR1 = TRIMMED_DIR
else:
    TRIMMED_DIR0 = TRIMMED_DIR
    TRIMMED_DIR1 = TRIMMED_DIR+"tmp"
if (not(config["SKIP_FASTQC_TRIMMED"])):
    SE_OUT = SE_OUT+[FASTQC_DIR+"/trimmedData/"+str(F)+"_trimmed_fastqc.html" for F in ALL_SAMPLES] #fastqc report for trimmed data
    PE_OUT = PE_OUT+[FASTQC_DIR+"/trimmedData/"+str(F)+"_trimmed_R1_fastqc.html" for F in ALL_SAMPLES]+[FASTQC_DIR+"/trimmedData/"+str(F)+"_trimmed_R2_fastqc.html" for F in ALL_SAMPLES] #fastqc report for trimmed data

rule all:
    input:
        # OUT_DIR+"/metaData.txt",
        PE_OUT if PAIRED_READS else SE_OUT

# rule printMetaInfo:
#     params:
#         config = config
#     output:
#         OUT_DIR+"/metaData.txt"
#     script:
#         "printMetaDataInfo.R"
    
def get_samples(wildcards):
    idx = samples.index[(samples['sample'] == wildcards.sample) & (samples['R1/R2']=="SE")].tolist()
    return(samples['sample'].loc[idx].tolist())

rule get_fq:
    input:
    output:
        FASTQ = FASTQ_DIR+"/{sample}/{sample}.fastq.gz"
    shell:
        """
        if [ {DOWNLOAD} == "True" ]; then
            # Download files from ENA
            module load Anaconda3
            {ENABROWSER_PROGRAM}/enaDataGet -f fastq -d {FASTQ_DIR} {wildcards.sample}
        else
            # Copy fastq.gz files from sample folder in local path to output dir
            cat {LOCAL_PATH}/{wildcards.sample}/*fastq.gz > {output}
        fi
        """

rule trimadapter_pe:
    input:
        R1 = FASTQ_DIR+"/{sample}/{sample}_R1.fastq.gz",
        R2 = FASTQ_DIR+"/{sample}/{sample}_R2.fastq.gz"
    output:
        R1 = TRIMMED_DIR+"/{sample}_trimmed_R1.fastq.gz",
        R2 = TRIMMED_DIR+"/{sample}_trimmed_R2.fastq.gz"
    params:
        SKIP = SKIP_TRIM,
        Q = MIN_QUALITY,
        L = MIN_LENGTH, 
        A = ADAPTER_SEQUENCE,
        S = STRINGENCY,
        N = TRIM_N,
        ER = ERROR_RATE
    threads:
        CUTADAPTER_THREADS
    resources:
        cpus = CUTADAPTER_THREADS
    shell:
        """
        if [ {params.SKIP} == "True" ]; then
            cp {input.R1} {output.R1};
            cp {input.R1} {output.R2};  
        else #cut adapter
            echo "Trimming {wildcards.sample}..."
            if [ {params.N} == "True" ]; then
                if [ {params.A} == "NA" ]; then
                    {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} -e {params.ER} --paired {input.R1} {input.R2} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                else  
                    {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} --adapter {params.A} -e {params.ER} --paired {input.R1} {input.R2} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                fi  
            else  
                if [ {params.A} == "NA" ]; then
                    {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} -e {params.ER} --paired {input.R1} {input.R2} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                else
                    {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} --adapter {params.A} -e {params.ER} --paired {input.R1} {input.R2} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                fi  
            fi
            mv {TRIMMED_DIR}/{wildcards.sample}_R1_val_1.fq.gz {output.R1}
            mv {TRIMMED_DIR}/{wildcards.sample}_R2_val_2.fq.gz {output.R2}
        fi
        """

rule fastqc_raw_pe:
    input:
        R1 = FASTQ_DIR+"/{sample}/{sample}_R1.fastq.gz",
        R2 = FASTQ_DIR+"/{sample}/{sample}_R2.fastq.gz"
    output:
        FASTQC_R1 = FASTQC_DIR+"/rawData/{sample}_R1_fastqc.html",
        FASTQC_R2 = FASTQC_DIR+"/rawData/{sample}_R2_fastqc.html",
    shell:
        """
        {FASTQC_PROGRAM} -o {FASTQC_DIR}/rawData {input.R1} {input.R2}
        """

rule fastqc_trimmed_pe:
    input:
        R1 = TRIMMED_DIR+"/{sample}_trimmed_R1.fastq.gz",
        R2 = TRIMMED_DIR+"/{sample}_trimmed_R2.fastq.gz"
    output:
        FASTQC_TRIMMED_R1 = FASTQC_DIR+"/trimmedData/{sample}_trimmed_R1_fastqc.html",
        FASTQC_TRIMMED_R2 = FASTQC_DIR+"/trimmedData/{sample}_trimmed_R2_fastqc.html"
    shell:
        """
        {FASTQC_PROGRAM} -o {FASTQC_DIR}/trimmedData {input.R1} {input.R2}
        """
     
rule trimadapter_se:
    input:
        R = FASTQ_DIR+"/{sample}/{sample}.fastq.gz"
    output:
        TRIMMED_DIR+"/{sample}_trimmed.fastq.gz"
    params:
        SKIP = SKIP_TRIM,
        Q = MIN_QUALITY,
        L = MIN_LENGTH,
        A = ADAPTER_SEQUENCE,
        S = STRINGENCY,
        N = TRIM_N,
        ER = ERROR_RATE
    threads:
        CUTADAPTER_THREADS    
    resources:
        cpus = CUTADAPTER_THREADS
    shell:
        """
        if [ {params.SKIP} == "True" ]; then
            cp {input} {output}
        else # cutadapt
            echo "Trimming {wildcards.sample}..."
            if [ {params.N} == "True" ]; then
                  if [ {params.A} == "NA" ]; then
                      {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} -e {params.ER} {input} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                  else  
                      {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --trim-n --stringency {params.S} --adapter {params.A} -e {params.ER} {input} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                  fi  
            else  
                if [ {params.A} == "NA" ]; then
                    {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} -e {params.ER} {input} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                else
                    {TRIMGALORE_PROGRAM} -j {threads} -q {params.Q} --length {params.L} --stringency {params.S} --adapter {params.A} -e {params.ER} {input} -o {TRIMMED_DIR} --path_to_cutadapt {CUTADAPT_PROGRAM}
                fi  
            fi
            mv {TRIMMED_DIR}/{wildcards.sample}_trimmed.fq.gz {output} 
        fi
        """
        
rule fastqc_raw_se:
    input:
        R = FASTQ_DIR+"/{sample}/{sample}.fastq.gz"
    output:
        FASTQC_DIR+"/rawData/{sample}_fastqc.html",
    shell:
        """
        {FASTQC_PROGRAM} -o {FASTQC_DIR}/rawData {input.R}
        """

rule fastqc_trimmed_se:
    input:
        R = TRIMMED_DIR+"/{sample}_trimmed.fastq.gz"
    output:
        FASTQC_DIR+"/trimmedData/{sample}_trimmed_fastqc.html"
    shell:
        """
        {FASTQC_PROGRAM} -o {FASTQC_DIR}/trimmedData {input.R}
        """

ruleorder: salmon_count_pe > salmon_count_se

rule salmon_count_pe:
    input:
        R1 = TRIMMED_DIR+"/{sample}_trimmed_R1.fastq.gz",
        R2 = TRIMMED_DIR+"/{sample}_trimmed_R2.fastq.gz"
    output:
        SALMON_DIR+"/{sample}.quant"
    threads:
        SALMON_THREADS
    resources:
        cpus = SALMON_THREADS
    shell:
        """
        {SALMON_PROGRAM} quant -p {SALMON_THREADS} -i {INDEX} -l A -1 {input.R1} -2 {input.R2} --validateMappings -o {SALMON_DIR}/{wildcards.sample}
        cp {SALMON_DIR}/{wildcards.sample}/quant.sf {output}
        """
              
rule salmon_count_se:
    input:
        R = TRIMMED_DIR+"/{sample}_trimmed.fastq.gz"
    output:
        SALMON_DIR+"/{sample}.quant"
    threads:
        SALMON_THREADS
    resources:
        cpus = SALMON_THREADS
    shell:
        """
        {SALMON_PROGRAM} quant -p {threads} -i {INDEX} -l A -r {input.R} --validateMappings -o {SALMON_DIR}/{wildcards.sample}
        cp {SALMON_DIR}/{wildcards.sample}/quant.sf {output}
        """

